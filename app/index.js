const express = require("express");
const router = require("../config/routes");
const app = express();
const port = 5000;
const cors = require("cors");
const YAML = require("yamljs");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = YAML.load("./openapi.yaml");

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Open API
app.use("/api/docs/json", (req, res) => {
  res.json({ swaggerDocument });
});
app.use("/api/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Router
app.use(router);

app.listen(port, () => {
  console.log(`Server running on localhost:${port}`);
});
